import React, {Component} from 'react';
import './App.css';
import {Layout, Header, Navigation, Drawer, Content} from "react-mdl";
import Main from './components/main';
import {Link} from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { CookiesProvider } from 'react-cookie';
import { useCookies } from 'react-cookie';
import { withCookies, Cookies } from 'react-cookie';
import * as cookie from "react-cookie";

class App extends Component {

    constructor(props) {
        super(props);


        this.handler_login = this.handler_login.bind(this);
        this.handler_logout = this.handler_logout.bind(this);

        this.state = {
            logged_in: false,
            user_id: 0,
            username: 'null',
            accesslevel: 'null',
            admin: null
        };
    }

    handler_login = (ID, usrname, group) => {
        this.setState({
            logged_in: true,
            user_id: ID,
            username: usrname,
            accesslevel: group
        });

        const cookies = new Cookies();

        cookies.set('logged_in', true, {path: '/'});
        cookies.set('user_id', ID, {path: '/'});
        cookies.set('group', group, {path: '/'});
        cookies.set('username', usrname, {path: '/'});

        if (this.state.accesslevel === "admin") {
            this.setState({
                admin: true
            });
        } else {
            this.setState({
                admin: false
            });
        }
    };

    handler_logout =(e) =>{
        this.setState({
            logged_in: "",
            user_id: "",
            username: "",
            accesslevel: "",
            admin: ""
        });

        const cookies = new Cookies();

        cookies.set('logged_in', "", {path: '/'});
        cookies.set('user_id', "", {path: '/'});
        cookies.set('group', "", {path: '/'});
        cookies.set('username', "", {path: '/'});

        this.forceUpdate();

    };




    componentDidMount() {

        const cookies = new Cookies();

        let ls = cookies.get('logged_in');
        let us = cookies.get('user_id');
        let gs = cookies.get('group');
        let un = cookies.get('username');

        if(gs === "admin"){
            this.setState({
                admin: true
            });
        }else{
            this.setState({
                admin: false
            });
        }

        this.setState({
            logged_in: ls,
            user_id: us,
            accesslevel: gs,
            username: un
        })

    }


    render() {

        let account_label = "Login";

        if(this.state.logged_in){
            account_label = "Account";
        }else{
            account_label = "Login";
        }


        return (
            <div className="demo-big-content" style={{overflow: 'scroll'}} style={{overflow: 'scroll'}}>
                <Layout>
                    <Header className="header-colour">

                        <Navigation>
                            <Link to="/">Home</Link>
                            {this.state.logged_in && <Link to="/progress">Progress</Link>}
                            <Link to="/about">About</Link>
                            <Link to="/account">{account_label}</Link>
                            <Link to={"/admin"} style={{display: this.state.admin ? 'block' : 'none' }}>Admin</Link>
                            {this.state.logged_in && <Link onClick={this.handler_logout} to="/">Logout</Link>}
                        </Navigation>

                    </Header>
                    <Drawer title="TrainingDay">
                        <Navigation>
                            <Link to="/">Home</Link>
                            {this.state.logged_in && <Link to="/progress">Progress</Link>}
                            <Link to="/about">About</Link>
                            <Link to="/account">{account_label}</Link>
                            <Link to={"/admin"} style={{display: this.state.admin ? 'block' : 'none' }}>Admin</Link>
                            {this.state.logged_in && <Link onClick={this.handler_logout} to="/">Logout</Link>}
                        </Navigation>
                    </Drawer>
                    <Content>
                        <div className="page-content">
                        <Main handler_login ={this.handler_login} logged_in={this.state.logged_in} username={this.state.username} accesslevel={this.state.accesslevel} user_id={this.state.user_id}/>
                        </div>
                    </Content>
                </Layout>
            </div>
        );
    }
}

export default App;
