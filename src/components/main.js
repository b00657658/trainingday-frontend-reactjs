import React, {Component} from 'react';
import About from './about';
import Account from './account';
import Progress from './Progress';
import {Switch, Route,} from 'react-router-dom';
import Dashboard from "./dashboard";
import Admin from "./admin";
import Task from "./show_task";

class Main extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <Switch logged_in={this.props.logged_in} handler_login={this.props.handler_login} user_id={this.props.user_id}>
                {/*<Route exact path="/" component={Dashboard}/>*/}
                <Route
                    exact path="/"
                    render={() => <Dashboard logged_in={this.props.logged_in} user_id={this.props.user_id}/>}
                />
                <Route path="/about" component={About}/>
                {/*<Route path="/account" component={Account} logged_in={props.logged_in} handler={props.handler}/>*/}
                <Route
                    path="/account"
                    render={() => < Account logged_in={this.props.logged_in} handler_login={this.props.handler_login} username={this.props.username} accesslevel={this.props.accesslevel}/>}
                />
                {/*<Route path="/progress" user_id={this.props.user_id} logged_in={this.props.logged_in} component={Progress}/>*/}

                <Route
                    path="/progress"
                    render={() => < Progress user_id={this.props.user_id} logged_in={this.props.logged_in}  username={this.props.username} accesslevel={this.props.accesslevel}/>}
                />

                <Route
                    path="/admin"
                    render={() => < Admin logged_in={this.props.logged_in} username={this.props.username} accesslevel={this.props.accesslevel}/>}
                />

                <Route
                        path="/task/:ID"
                       render={(props) => < Task {...props} logged_in={this.props.logged_in} user_id={this.props.user_id} username={this.props.username} accesslevel={this.props.accesslevel}/>}
                />


            </Switch>)
    }
}




export default Main;