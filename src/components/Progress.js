import React, { Component } from 'react'
import {Cell, Grid} from "react-mdl";
import axios from "axios";
import ProgressBar from "react-bootstrap/ProgressBar";


class Progress extends Component {

    constructor(props) {
        super(props);

        this.setState({
            all_tasks: []
        })

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        // Backup method to ensure that task data displays on first load.
        if(this.state.loaded){

        }else{
            axios.get("http://localhost:5000/api/user/tasks/" + this.props.user_id)
                .then(({data}) => {
                    this.setState({
                        tasks: data
                    });
                    this.setState({
                        loaded:true,
                        firstrun: false
                    });

                    const complete = this.state.tasks.filter(task  => task.complete === 'true');

                    if(!this.state.firstrun && this.state.tasks.length > 0){
                        this.setState({
                            assigned: this.state.tasks.length,
                            complete: complete.length,
                            progress: Math.floor((this.state.complete / this.state.assigned) * 100)
                        });

                    }
                });

            axios.get("http://localhost:5000/api/tasks/assigned")
                .then(({data}) => {
                    this.setState({
                        all_tasks: data
                    });

                    let all_complete = null;
                    if(this.state.all_tasks){
                        all_complete = this.state.all_tasks.filter(task  => task.complete === 'true');
                    }else{

                    }


                    if(!this.state.firstrun){
                        this.setState({
                            total_assigned: this.state.all_tasks.length,
                            total_complete: all_complete.length,
                            total_progress: Math.floor((this.state.total_complete / this.state.total_assigned) * 100)
                        });

                    }
                });

        }

    }


    componentDidMount(){

        this.setState({
            user_id: this.props.user_id,
            loaded: false,
            firstrun: true
        });

    }

    componentWillMount() {
        this.setState({
            assigned: 0,
            complete: 0,
            progress: 0,
            loaded: false,
            tasks: [],
            user_id: this.props.user_id
        })
    }

    render() {
        return(
            <div style={{width: '100%', margin: 'auto'}}>
                <Grid className="browse-grid">
                    <Cell col={12} style={{'height': '10%'}}>
                        <div className="banner-text">
                            <h1>Your Progress</h1>
                        </div>

                        <ProgressBar animated={true} max={this.state.assigned * 100} style={{'margin': '1em auto 0em auto', 'width':'58%', 'height':'6em', '-webkit-box-shadow':'1px 1px 8px 1px rgba(3,3,3,3)'}}>
                            <ProgressBar striped variant="warning" label={"Not Started: "+(Math.floor(((this.state.assigned-this.state.complete) / this.state.assigned)*100))+"%"} now={((this.state.assigned-this.state.complete) / this.state.assigned)*100} key={1} style={{'color':'black', 'font-size':'200%'}}/>
                            <ProgressBar striped variant="success" label={"Complete: "+(Math.floor((this.state.complete / this.state.assigned) * 100)+"%")} now={(this.state.complete / this.state.assigned) * 100} key={2} style={{'color':'black', 'font-size':'200%'}}/>

                        </ProgressBar>;

                    </Cell>

                    <div style={{display:'inline-block', 'margin':'1em auto 0em auto', 'padding-bottom':'1em', 'background':'azure', '-webkit-box-shadow':'1px 1px 8px 1px rgba(3,3,3,3)', 'border-radius':'12px', 'height':'30%', 'width':'60%'}}>


                        <h1 style={{margin: '0'}}>Your Stats</h1>

                        <div className="progress-box">
                            <div className="progress-box-title">Assigned</div>
                            <div className="progress-box-value">{this.state.assigned}</div>
                        </div>

                        <div className="progress-box">
                            <div className="progress-box-title">Complete</div>
                            <div className="progress-box-value">{this.state.complete}</div>
                        </div>

                        <div className="progress-box">
                            <div className="progress-box-title">Progress</div>
                            <div className="progress-box-value">{this.state.progress}%</div>
                        </div>
                    </div>


                    {/*{this.props.accesslevel === "admin" && <div className="progress-box-lower">*/}

                    {this.props.accesslevel === "admin" && <div style={{display:'inline-block', 'margin':'1em auto 0em auto', 'padding-bottom':'1em', 'background':'azure', '-webkit-box-shadow':'1px 1px 8px 1px rgba(3,3,3,3)', 'border-radius':'12px', 'height':'30%', 'width':'60%'}}>
                        <h1 style={{margin: '0'}}>Manager Stats</h1>

                        <div className="progress-box">
                            <div className="progress-box-title">Total Assigned</div>
                            <div className="progress-box-value">{this.state.total_assigned}</div>
                        </div>

                        <div className="progress-box">
                            <div className="progress-box-title"> Total Complete</div>
                            <div className="progress-box-value">{this.state.total_complete}</div>
                        </div>

                        <div className="progress-box">
                            <div className="progress-box-title">Total Progress</div>
                            <div className="progress-box-value">{this.state.total_progress}%</div>
                        </div>
                    </div>}

                {/*</div>*/}

                </Grid>
            </div>
        )
    }
}
export default Progress;