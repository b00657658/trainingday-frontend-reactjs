import React, { Component } from 'react'
import {Cell, Grid} from "react-mdl";
import Button from 'react-bootstrap/Button';
import AddTask from './add_task';
import AssignTraining from "./assign_training";
import ReactTable from 'react-table'
import axios from "axios";
import 'react-table/react-table.css'
import Alert from 'react-bootstrap/Alert'

class Admin extends Component {

    constructor(props) {
        super(props);

        this.state = {
            show_create_task: false,
            show_assign_task: false,
            show_all_assignments: false,
            alert_assign: false,
            alert_create: false
        };

        this.updateVisible = this.updateVisible.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleHide = this.handleHide.bind(this);
        this.handleShow = this.handleShow.bind(this);

    }

    getAssignments(){
        axios.get("http://localhost:5000/api/tasks/assigned")
            .then(({data}) => {
                this.setState({
                    all_tasks: data
                });

                const all_complete = this.state.all_tasks.filter(task  => task.complete === 'true');

            });
    }

    updateVisible = (e) =>{
        e.preventDefault();
        if(!this.state[e.target.value]) {
            this.setState({
                [e.target.value]: true
            });
        }else{
            this.setState({
                [e.target.value]: false
            });
        }

        if(e.target.value === "show_all_assignments"){

            this.getAssignments();

        }

    };

    handleDelete(assignment){

        this.setState({
            unassign: assignment
        });

        const payload = {
            USRNM: assignment.username,
            TNM: assignment.task_name
        };

        const config = {
            headers: { 'content-type': 'application/json', 'Access-Control-Allow-Origin' : '*'}
        };

        axios.post('http://localhost:5000/api/task/unassign', payload, config)
            .then(response => {
                console.log(response);
                this.getAssignments();
            })
            .catch(error => {
                console.log(error);
            });


    };

    handleHide =(e)=>{
        this.setState({
            [e.target.value]: false
        });
    };

    handleShow = (e) =>{
        this.setState({
            [e.target.value]: true
        });
    };

    handleAlert(){
        let alert_body="Loading...";
        let alert_title="Loading...";

        if(this.state.alert_assign){
             alert_body = "Task has been assigned!";
             alert_title = "Task Assigned"
        }else if(this.state.alert_create){
            alert_body = "Task has been created!";
            alert_title = "Task Created"
        }

        return(
            <Alert show={(this.state.alert_assign || this.state.alert_create)} variant="success" style={{'width':'60%', 'height':'5em', 'margin':'auto'}}>
                <Alert.Heading>{alert_title}</Alert.Heading>
                <p>
                    {alert_body}
                </p>
                {this.state.alert_assign && <Button name="alert_assign" value="alert_assign" onClick={this.handleHide} variant="outline-success" style={{'float':'right', position:'relative', bottom:'4.4em'}}>
                    Close
                </Button>}
                {this.state.alert_create && <Button name="alert_create" value="alert_create" onClick={this.handleHide} variant="outline-success" style={{'float':'right', position:'relative', bottom:'4.4em'}}>
                    Close
                </Button>}
            </Alert>

        );
    }


    render(){

        const columns = [{
            Header: 'Task Name',
            accessor: 'task_name'
        }, {
            Header: 'User',
            accessor: 'username',
        }, {
            Header: 'Completed',
            accessor: 'complete',
            Cell: props => <span className="complete">
                {props.value === "false" &&
                <i className="material-icons" style={{'position': 'absolute', 'color': 'red'}}>not_interested</i>}
                <i className="material-icons" style={{'color': 'green'}}>done</i></span>,
        },{
            Header: '',
            filterable: false,
            sortable: false,
            Cell: row => (
                <div>
                    <Button onClick={() => this.handleDelete(row.original)}>Un-assign</Button>
                </div>
            )
        }];

        return(
            <div className="admin-page">


                <Grid className="browse-grid">
                    <Cell col={12} row={1}>
                        <div className="banner-text">
                            <h1>Admin Controls</h1>
                        </div>

                        <div className="admin-container">


                            <Button onClick={this.updateVisible}
                                    variant="warning" size="lg"
                                    block
                                    value="show_create_task"
                                    style={this.state.show_create_task ? {background: 'coral'} : null}
                            >
                                Add New Training Task
                            </Button>
                            <Button onClick={this.updateVisible}
                                    variant="warning" size="lg"
                                    block
                                    value="show_assign_task"
                                    style={this.state.show_assign_task ? {background: 'coral'} : null}
                            >
                                Assign Training Record
                            </Button>
                            <Button onClick={this.updateVisible}
                                    variant="warning"
                                    size="lg"
                                    block
                                    value="show_all_assignments"
                                    style={this.state.show_all_assignments ? {background: 'coral'} : null}
                            >
                                Show all users assignments status
                            </Button>

                        </div>

                        {(this.state.show_create_task || this.state.show_assign_task) && <div style={{overflow: 'visible', 'padding-bottom': '1%', 'padding-top': '1%', 'width': '100%', 'margin': 'auto'}}>
                            {(this.state.alert_assign || this.state.alert_create) && this.handleAlert()}
                            {this.state.show_create_task && <AddTask handle_show={this.handleShow} />}
                            {this.state.show_assign_task && <AssignTraining handle_show={this.handleShow} style={{'padding-top': '1%', 'margin-top' : '1em'}}/>}

                        </div>}
                    </Cell>

                    <Cell col={12} row={2}>
                        {this.state.show_all_assignments && <div style={{'display':'block','position':'relative', 'padding-top':'0'}}><h1>All Assignments</h1></div>}
                        {this.state.show_all_assignments && <ReactTable
                            className="all-tasks-table"
                            data={this.state.all_tasks}
                            columns={columns}
                            defaultPageSize={10}
                            resolveData={data => data.map(row => row)}
                            filterable={true}
                            style={{
                                'background':'#f1dbf3c7',
                                'width': '60%',
                                'margin': '0 auto 5% auto'
                            }}
                        />}
                    </Cell>

                </Grid>

            </div>
        )
    }
}
export default Admin;