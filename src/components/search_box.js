import React, { Component } from 'react'
import {Grid, Cell, Tabs, Card, CardTitle, CardText, CardActions, Button, Textfield} from "react-mdl";
import { InputGroup, InputGroupText, InputGroupAddon, Input } from 'reactstrap';

class SearchBox extends Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {

    }

    updateSearch = (e) => {

    };


    render() {
        return(
            <div className="search-box">

                <InputGroup onChange={this.props.updateSearch} style={{'position':'relative'}}>
                    <Input placeholder={"Search"} style={{height: 'auto', 'font-size':'2em'}}/>
                    <InputGroupAddon addonType="append">
                        <InputGroupText>Search</InputGroupText>
                    </InputGroupAddon>
                </InputGroup>

            </div>
        )
    }
}
export default SearchBox;