import React, {Component} from 'react';
import ReactSignupLoginComponent from 'react-signup-login-component';
import axios from 'axios';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';
import Login from "./login";

const LoginPage = (props) => {

    let title = "Login";

    const signupWasClickedCallback = (data) => {

        let jdata = JSON.stringify(data);
        delete jdata.passwordConfirmation;

        console.log(jdata);

        axios.post('http://localhost:5000/api/users', jdata, {
            headers: {'Content-Type': 'application/json'}
        })
            .then(res => {
                console.log(res);
                console.log(res.data);
                if(res.statusText === "CREATED"){
                    console.log("User created.");
                    alert("Account created");
                }
            })
            .catch(error =>{
                alert("Account already exists");
            });

    };


    const config = {
        headers: { 'content-type': 'application/json', 'Access-Control-Allow-Origin' : '*'}
    };

    const loginWasClickedCallback = (data) => {
        console.log(data);

        console.log(data.username);

        const payload = {
            password: data.password
        };

        axios.post('http://localhost:5000/api/login2/'+data.username, payload, config)

            .then(response => {
                if(response.data ===""){alert("Login Failed")};
                console.log(response.data[0].ID);
                console.log(response.data[0].username);
                console.log(response.data[0].accesslevel);

                if(response.data.length > 0){
                    props.handler_login(response.data[0].ID, response.data[0].username, response.data[0].accesslevel)
                }



            })
            .catch(error => {
                console.log(error);
            });


        // OLD LOGIN METHOD FOR UN-SECURED PASSWORDS
        // axios.get("http://localhost:5000/api/login/"+data.username)
        //     .then(function (response) {
        //         console.log(response.data.password);
        //         console.log(response.data.ID);
        //         console.log(response.data.accesslevel);
        //
        //
        //         if(data.password === response.data.password){
        //             props.handler_login(response.data.ID, data.username, response.data.accesslevel);
        //             title = 'Logged in';
        //         }else{
        //             title = "Failed Login";
        //         }
        //
        //     });


    };

    const recoverPasswordWasClickedCallback = (data) => {
        console.log(data);
        alert('Recover password callback, see log on the console to see the data.');
    };

    if(!props.logged_in) {
        return (
            <div>
                <ReactSignupLoginComponent
                    title="Please Login or Register"
                    handleSignup={signupWasClickedCallback}
                    handleLogin={loginWasClickedCallback}
                    handleRecoverPassword={recoverPasswordWasClickedCallback}
                    submitLoginCustomLabel="Login"
                    submitRecoverPasswordCustomLabel="Recover"
                    goToSignupCustomLabel="Register"
                    goToLoginCustomLabel="Already have an account?"

                    styles={{
                        mainWrapper: { backgroundColor: 'linear-gradient(to left, #12c2e9, #c471ed)'},
                        mainTitle: {}
                    }}


                />
            </div>
        );
    }else{
        return(<div>    </div>);
    }
};

export default LoginPage;