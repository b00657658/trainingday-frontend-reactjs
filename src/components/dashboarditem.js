import React, { Component } from 'react'
import {Grid, Cell, Tabs, Card, CardTitle, CardText, CardActions, Button, Textfield} from "react-mdl";
import Link from "react-router-dom/Link";

class Dashboarditem extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return(
            <li key={this.props.ID}>
                    <Card style={{'border-radius': '12px', '-webkit-box-shadow':'1px 1px 8px 1px rgba(3,3,3,3)'}}>
                        <CardTitle expand style={{
                            padding: '10px',
                            color: 'black', height:'50px',
                            backgroundPosition: 'center',
                            'font-size': '2em',
                            backgroundImage:"url()" }}>{this.props.task_name.toUpperCase()}{this.props.complete === "true" && <i className="material-icons" style={{'color': 'green', 'font-size': '1em'}}>done</i>}
                            </CardTitle>

                        <CardText style={{color: 'black', 'font-size': '1em'}}>
                            {this.props.task_desc}
                        </CardText>

                        <CardActions border>
                            <Button>
                                <Link to={`/task/${this.props.ID}`}> View Assignment </Link>
                            </Button>
                        </CardActions>
                    </Card>
            </li>
        )
    }
}
export default Dashboarditem;