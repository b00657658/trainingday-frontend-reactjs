import React, { Component } from 'react'
import {Grid, Cell, Tabs, Card, CardTitle, CardText, CardActions, Button, Textfield} from "react-mdl";
import Dashboarditem from "./dashboarditem";
import SampleJSON from "./sample.json";
import axios from "axios";

class Dashboarditems extends Component {

    constructor(props) {
        super(props);

        this.state = ({
            loaded: false,
            user_id: "",
            search_filtered: false
        });

        this.state = ({
            tasks: [{
                ID: "0",
                task_name: "Loading....",
                task_desc: "Loading....",
            }]
        });

    }

    componentWillMount() {

        // this.forceUpdate();
    }


    componentDidMount() {

        axios.get("http://localhost:5000/api/user/tasks/" + this.props.user_id)
            .then(({data}) => {
                this.setState({
                    tasks: data
                });
                // alert("DATA SUCCESS")
            });

        this.forceUpdate();

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        // Backup method to ensure that task data displays on first load.
        if(this.state.tasks.length > 0 && this.state.loaded){

        }else{
            axios.get("http://localhost:5000/api/user/tasks/" + this.props.user_id)
                .then(({data}) => {
                    this.setState({
                        tasks: data
                    });
                    this.setState({
                        loaded:true
                    })
                });

        }
    }


    render() {

        let json = this.state.tasks;

        if(this.props.search !== "Search" && this.props.search.length !== 0) {

            console.log("Searching for: "+this.props.search);
            console.log(this.state.tasks);
            let search_value = this.props.search;

            //Old filter method only checks for exact matches.
            // const filtered = this.state.tasks.filter(item => item.task_name === search_value);
            const filtered = this.state.tasks.filter(item => item.task_name.toLowerCase().indexOf(search_value.toLowerCase()) !== -1 );

            console.log(filtered);
            json = filtered;
        }


        let arr = [];


        Object.keys(json).forEach(function (key) {
            arr.push(json[key]);
            // console.log(key);
        });

        if(arr.length > 0){
            return(
                <ul className="event-cards">
                    {arr.map(item => <Dashboarditem ID={item.ID} task_name={item.task_name} task_desc={item.task_desc} complete={item.complete}/>)}
                </ul>
            )
        }else {
            return (
                <h1>No content found</h1>
            )
        }


    }
}
export default Dashboarditems;