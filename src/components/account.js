import React, { Component } from 'react'
import {Cell, Grid} from "react-mdl";
import Login from "./login"
import {Switch} from "react-router-dom";
import Main from "../App";

 class Account extends React.Component {

     constructor(props){
         super(props);

     }


     render() {

        if(!this.props.logged_in){
            return (
                <div style={{width: '100%', margin: 'auto'}}>
                    <Grid className="account-grid">
                        <Cell col={12}>
                            <div className="banner-text">
                                <h1>Account</h1>
                            </div>
                            <Login logged_in={this.props.logged_in} handler_login={this.props.handler_login}/>
                        </Cell>
                    </Grid>
                </div>
            )

        }else{
            return(
                <div style={{width: '100%', margin: 'auto'}}>
                    <Grid className="account-grid">
                        <Cell col={12}>
                            <div className="banner-text">
                                <h1>Account Info</h1>
                            </div>

                            <div className="account-container">

                                <div className="container">
                                    <div className="row username-container" style={{'margin':'0 50% 0 0'}}>
                                        <div className="col-md-3" style={{'text-align': 'right'}}><p>Username:  </p></div>
                                        <div className="col-md-9"  style={{'text-align': 'left'}}><p>{this.props.username.toUpperCase()}</p></div>
                                    </div>
                                    <div className="row group-container" style={{'margin':'0 50% 0 0'}}>
                                        <div className="col-md-3" style={{'text-align': 'right'}}><p>Group: </p></div>
                                        <div className="col-md-9" style={{'text-align': 'left'}}><p>{this.props.accesslevel.toUpperCase()}</p></div>
                                    </div>
                                </div>

                            </div>

                        </Cell>
                    </Grid>
                </div>
            )
        }


     }
 }

export default Account;