import React, { Component } from 'react'
import {Grid, Cell, Tabs, Card, CardTitle, CardText, CardActions, Button, Textfield} from "react-mdl";
import axios from "axios";
import Checkbox from '@material-ui/core/Checkbox';
import Form from 'react-bootstrap/Form'


class Task extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ID: null,
            task: [],
            task_file: [],
            assignment_file: [],
            file_downloaded: false,
            completion_downloaded: false,
            assignment_ID: null,
            user_ID: null,
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }


    componentDidMount() {


            const {match: {params}} = this.props;
            // alert(params.ID);

            this.setState({
                ID: params.ID,
                user_ID: this.props.user_id
            });


    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if(!this.state.task.ID){
            axios.get("http://localhost:5000/api/task/"+this.state.ID)
                .then(({data}) => {
                    this.setState({
                        task: data
                    });
                    console.log(this.state.task.task_name);
                });

        }

        this.getFile();
        this.getCompletion();
    }

    getCompletion(){

        if(!this.state.completion_downloaded) {

            const task_data = {
                USRID: parseInt(this.props.user_id),
                TID: parseInt(this.state.ID)
            };

            const config = {
                headers: {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
            };

            axios.post('http://localhost:5000/api/user/task/status', task_data, config)
                .then(({data}) => {
                    this.setState({
                        assignment_file: data,
                        completion_downloaded: true,
                        task_complete: this.state.assignment_file.complete
                    })
                })
                .catch(error => {
                    console.log(error);
                });

        }
    }

    getFile(){
        if(!this.state.file_downloaded){
            axios.get("http://localhost:5000/api/task/file/"+this.state.task.ID)
            .then(({data}) => {
                this.setState({
                    task_file: data,
                    file_downloaded: true
                });
            });
        }

    }

    onChange = (e) => {
      this.setState({
          [e.target.name]: e.target.checked
      });
    };

    onSubmit = (e) => {
        e.preventDefault();

        if(this.state.completion_check){
            const task_data = {
                USRID: parseInt(this.props.user_id),
                TID: parseInt(this.state.ID)
            };

            this.setState({
               task_complete: true
            });

            const config = {
                headers: { 'content-type': 'application/json', 'Access-Control-Allow-Origin' : '*'}
            };

            axios.post('http://localhost:5000/api/user/task/status/complete', task_data, config)
                .then(response => {
                    console.log(response);
                })
                .catch(error => {
                    console.log(error);
                });
        }else{
            alert("You must first check the completion confirmation box!");
        }


    };


    render() {

        if (this.state.task_complete === "false") {
            return (
                <div style={{'height': '100%'}}>
                    <div className="show-file">


                        {/*<h1>ID: {this.state.task.ID}</h1>*/}

                        <h2>TASK NAME: {this.state.task.task_name}</h2>

                        <p style={{'font-size': '30px'}}>{this.state.task.task_desc}</p>

                        <object data={this.state.task_file.file_path} className="file-object" type="application/pdf"
                                width="100%" height="100%" style={{'object-fit': 'cover'}}>

                        </object>
                    </div>

                    <Form className="task-form" onSubmit={this.onSubmit} style={{'margin-bottom':'1em'}}>
                    <Form.Group controlId="task-complete">
                    <Form.Label>Complete this task</Form.Label>
                    <Form.Text className="text-muted">
                    By checking this box you agree that this task has been completed and can be viewed as such by your manager.
                    </Form.Text>
                    </Form.Group>

                    <Form.Group required name="completion_check" controlId="form-complete-task">
                    <Form.Check onChange={this.onChange} name="completion_check" type="checkbox" label="Mark as complete" />
                    </Form.Group>
                    <Button variant="primary" type="submit" style={{'background':'#73e80aa3'}}>
                    Complete
                    </Button>
                    </Form>

                </div>
            )
        } else{
            return (
                <div style={{'height': '100%'}}>
                    <div className="show-file">


                        {/*<h1>ID: {this.state.task.ID}</h1>*/}

                        <h2>TASK NAME: {this.state.task.task_name}</h2>

                        <p style={{'font-size': '30px'}}>{this.state.task.task_desc}</p>

                        <object data={this.state.task_file.file_path} className="file-object" type="application/pdf"
                                width="100%" height="100%" style={{'object-fit': 'cover'}}>
                        </object>
                    </div>

                    <Form className="task-form">
                    <Form.Group controlId="task-complete">
                    <Form.Label>You have already completed this task.</Form.Label>
                    <Form.Text className="text-muted">
                    This content can be viewed but as completion has already been recorded this task requires no further action.
                    </Form.Text>
                    </Form.Group>
                    </Form>;
                </div>
            );
        }
    }



}
export default Task;