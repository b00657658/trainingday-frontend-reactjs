import React, { Component } from 'react'
import {Cell, Grid} from "react-mdl";
import Button from 'react-bootstrap/Button';
import axios from 'axios'

class AddTask extends Component {

    constructor(props) {
        super(props);

        this.state = {
            task_name: '',
            task_desc: '',
            image: null,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);

    }

    onChange = (e) =>{

        if(e.target.name === "image"){
            let extension = e.target.value.split('.').pop();
            if(extension !== "pdf"){
                alert("You did not select a PDF file!");
                e.target.value = "";
            }
        }

        this.setState({
            [e.target.name]: e.target.value
        });
    };


    handleSubmit(e){
        e.preventDefault();

        const formData = new FormData();
        formData.append("image", this.uploadInput.files[0]);
        formData.append("task_name", this.state.task_name);
        formData.append("task_desc", this.state.task_desc);

        const config = {
            headers: { 'content-type': 'multipart/form-data', 'Access-Control-Allow-Origin' : '*'}
        };

        axios.post('http://localhost:5000/api/upload', formData, config)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });

        this.setState({
            task_name: '',
            task_desc: '',
            image: null,
        })

    }


    render(){
        return(
            <div className="add-task">
                <form encType="multipart/form-data" className="form-horizontal" onSubmit={this.handleSubmit} method="post" style={{'width':'300%'}}>
                    <fieldset>
                        <legend>Create new training task</legend>

                        <div className="form-group">
                            <label className="col-md-4 control-label" htmlFor="task_name">Title</label>
                            <div className="col-md-4">
                                <input id="task_name" value={this.state.task_name} name="task_name" type="text" placeholder="Health and Safety Document" className="form-control input-md" required onChange={this.onChange}/>

                            </div>
                        </div>


                        <div className="form-group">
                            <label className="col-md-4 control-label"
                                   htmlFor="task_desc">Description</label>
                            <div className="col-md-4">
                                <textarea placeholder={"Provide a basic description of the task..."} value={this.state.task_desc} className="form-control" id="task_desc" name="task_desc" defaultValue={"Basic task description..."} required onChange={this.onChange} />
                            </div>
                        </div>


                        <div className="form-group">
                            <label className="col-md-4 control-label" htmlFor="file">Select File</label>
                            <div className="col-md-4">
                                <input value={this.state.image} ref={(ref) => { this.uploadInput = ref; }} id="image" name="image" className="input-file" type="file" accept="application/pdf" required onChange={this.onChange} />
                            </div>
                        </div>


{/*
                        <div className="form-group">
                            <label className="col-md-4 control-label" htmlFor="task_date">Date Due</label>
                            <div className="col-md-4">
                                <div className="input-group">
                                    <span className="input-group-addon">Date</span>
                                    <input id="task_date" name="task_date" className="form-control" placeholder="11/03/20" type="text" onChange={this.onChange}/>
                                </div>
                                <p className="help-block">DD/MM/YY</p>
                            </div>
                        </div>
*/}

                        <div className="form-group" >
                            <label className="col-md-4 control-label" htmlFor="submit">Submit</label>
                            <div className="col-md-4">
                                <button value="alert_create" onClick={(this.state.task_name && this.state.task_desc && this.state.image) && this.props.handle_show} id="submit" name="submit" className="btn btn-success" style={{'width':'100%'}}>Go</button>
                            </div>
                        </div>

                    </fieldset>
                </form>

            </div>
        )
    }
}
export default AddTask;