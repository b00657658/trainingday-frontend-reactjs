import React, { Component } from 'react';
import {Cell} from "react-mdl";
import LoginPage from "./login_form";
import Main from "../App";

class Login extends React.Component {

    constructor(props){
        super(props);

    }



    render() {
        return (
            <div className="banner-text-login">
                <div className="login-form-container">
                    <LoginPage logged_in={this.props.logged_in} handler_login={this.props.handler_login}/>
                </div>
            </div>

        );
    }
}

export default Login;