import React, {Component} from 'react'
import {Grid, Cell, Tabs, Card, CardTitle, CardText, CardActions, Button, Textfield} from "react-mdl";
import Logo from '../img/logo_small.png';
import Dashboarditems from "./dashboarditems";
import SearchBox from "./search_box";
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';
import Link from "react-router-dom/Link";

class Dashboard extends Component {
    constructor(props){
        super(props);

        this.updateSearch = this.updateSearch.bind(this);

        this.state = {
            search: "Search"
        }

    }

    updateSearch = (e) => {
        this.setState({
            search: e.target.value
        })
    };


    render() {

        if(this.props.logged_in) {
            return (
                <div>
                    <Grid style={{'overflow-x': 'hidden'}} className="landing-grid">
                        <Cell col={12} style={{display: this.props.logged_in ? 'block' : 'none'}}>
                            <img
                                src={Logo} alt="logo"
                                className="logo-img"
                            />
                            <div className="banner-text">
                                <SearchBox updateSearch={this.updateSearch}/>
                            </div>

                            <div className="banner-card">
                                <h1>Assigned Modules</h1>

                                <Dashboarditems user_id={this.props.user_id} logged_in={this.props.logged_in}
                                                search={this.state.search}/>

                            </div>

                        </Cell>
                    </Grid>
                </div>
            )
        }else{
            return(
                <div style={{margin: '10% 0 0 0', 'text-align': 'center'}}>

                    <img
                        src={Logo} alt="logo"
                        className="logo-img"
                    />

                    <h1>Please login.</h1>


                    <Link to={`/account`}> <Button raised accent ripple>TO LOGIN</Button></Link>

                </div>
            );
        }


    }
}

export default Dashboard;