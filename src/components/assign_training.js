import React, { Component } from 'react'
import {Cell, Grid} from "react-mdl";
import Button from 'react-bootstrap/Button';
import axios from 'axios'
import {string} from "prop-types";

class AssignTraining extends Component {

    constructor(props) {
        super(props);

        this.state = {
            task_name: '',
            task_desc: '',
            user_data: [{'ID': 0, 'username': 'Loading...'}],
            task_data: [{'ID': 0, 'task_name': 'Loading...'}],
            all_tasks: [],
            USRID: 0,
            TID: 0
        };

        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentWillMount() {

        axios.get("http://localhost:5000/api/users")
            .then(({data}) => {
                this.setState({
                    user_data: data
                });
                console.log(this.state.user_data[0].username);
            });


        axios.get("http://localhost:5000/api/tasks")
            .then(({data}) => {
                this.setState({
                    task_data: data
                });
                console.log(this.state.user_data[0].task_name);
        });

    }

    onChange = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    handleSubmit(e){
        e.preventDefault();

        const assignment = {
            USRID: parseInt(this.state.user),
            TID: parseInt(this.state.task)
        };

        const config = {
            headers: { 'content-type': 'application/json', 'Access-Control-Allow-Origin' : '*'}
        };

        axios.post('http://localhost:5000/api/task/assign', assignment, config)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });

        this.setState({
            user: "",
            task: ""
        })


    }

    render(){

        let {users} = this.state.user_data;

        return(
            <div className="assign-training">

                <form className="form-horizontal" onSubmit={this.handleSubmit} style={{'width':'300%'}}>
                    <fieldset>


                        <legend>Assign training to user</legend>


                        <div className="form-group">
                            <label className="col-md-4 control-label" htmlFor="task">Select Task</label>
                            <div className="col-md-4">
                                <select id="task" onChange={this.onChange} defaultValue="select" name="task" className="form-control" value={this.state.task} required>
                                    <option value="select">None selected</option>
                                    {this.state.task_data.map((task) => <option key={task.ID} value={task.ID}>{task.task_name}</option>)}
                                </select>
                            </div>
                        </div>


                        <div className="form-group">
                            <label className="col-md-4 control-label" htmlFor="user">Select User</label>
                            <div className="col-md-4">
                                <select id="user" defaultValue="select" onChange={this.onChange} name="user" className="form-control" value={this.state.user} required>
                                    <option value="select">None selected</option>
                                    {this.state.user_data.map((user) => <option key={user.ID} value={user.ID}>{user.username}</option>)}
                                </select>
                            </div>
                        </div>


                        <div className="form-group" style={{'padding-top':'6em'}}>
                            <label className="col-md-4 control-label" htmlFor="submit">Assign Task</label>
                            <div className="col-md-4">
                                <button id="submit" value="alert_assign" onClick={(this.state.user && this.state.task) && this.props.handle_show} name="submit" className="btn btn-primary" style={{'width':'100%'}}>Assign</button>
                            </div>
                        </div>

                    </fieldset>
                </form>



            </div>
        )
    }
}
export default AssignTraining;